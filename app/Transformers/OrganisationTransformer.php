<?php

declare(strict_types=1);

namespace App\Transformers;

use App\Organisation;
use App\User;
use League\Fractal\TransformerAbstract;


/**
 * Class OrganisationTransformer
 * @package App\Transformers
 */
class OrganisationTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'user'
    ];

    /**
     * @param Organisation $organisation
     *
     * @return array
     */
    public function transform(Organisation $organisation)
    {
        /** @var User $user */
        $user = User::find($organisation->owner_user_id);
        $user = (new UserTransformer)->transform($user);

        return [
            'id' => (int) $organisation->id,
            'name' => $organisation->name,
            'user' => $user,
            'expires' => $organisation->trial_end,
            'subscribed' => (int) $organisation->subscribed
        ];
    }

    /**
     * @param Organisation $organisation
     *
     * @return \League\Fractal\Resource\Item
     */
    public function includeUser(Organisation $organisation)
    {
        return $this->item($organisation->user, new UserTransformer());
    }
}
