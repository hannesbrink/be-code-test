<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Organisation;
use App\User;
use App\Services\OrganisationService;
use App\Transformers\OrganisationTransformer;
use App\Transformers\UserTransformer;

use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use League\Fractal;
use Auth;
use Log;

/**
 * Class OrganisationController
 * @package App\Http\Controllers
 */
class OrganisationController extends ApiController
{

    /**
     * @param OrganisationService $service
     *
     * @return JsonResponse
     */
    public function create(OrganisationService $service): JsonResponse
    {

        if($this->request->has('name') && $this->request->input('name') != ''):

            /** @var Organisation $organisation */
            $organisation = $service->createOrganisation($this->request->all());
            
            /** @var User $user */
            $user = auth()->guard('api')->user();

            return $this
                ->transformItem('organisation', $organisation, $user)
                ->respond();

        else:

            /** @var JsonResponse $jsonresponse */
            $jsonresponse = new JsonResponse( array("Please provide the organisation's name.") );
            return $jsonresponse;

        endif;
    }

    /**
     * @param OrganisationService $service
     *
     * @return JsonResponse
     */
    public function listAll(OrganisationService $service): JsonResponse
    {
        /** @var GET $filter */
        $filter = (isset($_GET['filter']))? $_GET['filter']: false;
        
        /*$Organisations = DB::table('organisations')->get('*')->all();
        $Organisation_Array = array();
        for ($i = 2; $i < count($Organisations); $i -=- 1) {
            foreach ($Organisations as $x) {
                if (isset($filter)) {
                    if ($filter = 'subbed') {
                        if ($x->subscribed == 1) {
                            array_push($Organisation_Array, $x);
                        }
                    } else if ($filter = 'trail') {
                        if ($x['subbed'] == 0) {
                            array_push($Organisation_Array, $x);
                        }
                    } else {
                        array_push($Organisation_Array, $x);
                    }
                } else {
                    array_push($Organisation_Array, $x);
                }
            }
        }*/

        /** @var User $user */
        $user = auth()->guard('api')->user();
        
        /** @var Organisation $organisation */
        $organisations = $service->getOrganisations($filter);
         
        return $this
        ->transformCollection('organisations', $organisations, $user)
        ->respond();

    }
}
