<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;
use App\Mail\OrganisationCreated;
use App\User;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $organisations = array();
        $dbitems = DB::table('organisations')->get('*')->all();

        foreach($dbitems as $dbitem):
            $organisations[] = array(
                'name' => $dbitem->name,
                'owner' => User::find($dbitem->owner_user_id)->name,
                'subscribed' => ($dbitem->subscribed == 1)? 'Yes':'No',
                'trial_end' => $dbitem->trial_end,
            );
        endforeach;

        //Log::debug( print_r($organisations,true) );

        return view('home', compact('organisations'));
    }

    /**
     * Returns view used to show html form to create organisation.
     *
     * @return view
     */
    public function createOrganisation()
    {
        return view('homeorganisation');
    }

    /**
     * Store organisation created from form.
     * 
     * @param Request $request
     *
     * @return $status
     */
    public function storeOrganisation(Request $request)
    {
        request()->validate([
            'organisation_name' => 'required|max:255',
        ]);
        
        $user = Auth::user();
        $data = array();
            
        if($request->has('organisation_name')){
            $data['name'] = $request->input('organisation_name');
            $data['owner_user_id'] = $user->id;
            $data['trial_end'] = date('Y-m-d h:i:s', strtotime(' + 30 days'));
            $data['subscribed'] = 1;
            $data['created_at'] = date('Y-m-d h:i:s', time());
            $data['updated_at'] = date('Y-m-d h:i:s', time());
        }

        if(!empty($data)){
            DB::table('organisations')->insert($data);
            $status = 'Organisation created successfully.';
        } else {
            $status = 'Organisation creation failed.';
        }

    
        return back()->with('status', $status);
    }
}
