<?php

declare(strict_types=1);

namespace App\Services;

use Illuminate\Support\Facades\Mail;
use App\Mail\OrganisationCreated;
use App\Organisation;


/**
 * Class OrganisationService
 * @package App\Services
 */
class OrganisationService
{
    /**
     * @param array $attributes
     *
     * @return Organisation
     */
    public function createOrganisation(array $attributes): Organisation
    {
        /** @var User $user */
        $user = auth()->guard('api')->user();

        $organisation = new Organisation();
        $organisation->name = $attributes['name'];
        $organisation->owner_user_id = $user->id;
        $organisation->trial_end = date('Y-m-d h:i:s', strtotime(' + 30 days'));
        $organisation->subscribed = 1;
        $organisation->save();

        Mail::to($user->email)->send(new OrganisationCreated('Organistion '.$attributes['name'].' Created', $attributes['name'].' was created successfully.'));

        return $organisation;
    }

    /**
     * @param string $filter
     *
     * @return Organisation Collection
     */
    public function getOrganisations($filter)
    {
        if(!$filter){
            /** @var Organisation $organisations */
            $organisations = Organisation::all();
        } else {
            if($filter == 'subbed'){
                /** @var Organisation $organisations */
                $organisations = Organisation::where('subscribed', 1)->get();
            }
            if($filter == 'trial'){
                /** @var Organisation $organisations */
                $organisations = Organisation::where('subscribed', 0)->get();
            }
        }
        return $organisations;
    }

}
