<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// used to get access token for testing other api endpoints.
Route::post('login', 'AuthController@authenticate');

// simple test to see if access token and api authentication works.
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//organisation endpoints to listAll organisations and also create a new organisation using api authentication.
Route::prefix('organisation')->group(function () {
    Route::get('', 'OrganisationController@listAll')->middleware('auth:api');
    Route::post('', 'OrganisationController@create')->middleware('auth:api');
});
