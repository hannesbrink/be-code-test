<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// welcome page route.
Route::get('/', function () {
    return view('welcome');
});
// user authentication routes.
Auth::routes();

// user dashboard route to list all organisations.
Route::get('/home', 'HomeController@index')->name('home');

// user dashboard route to create an organisation.
Route::get('/home/organisation/create', 'HomeController@createOrganisation')->name('home.organisation.create');

// dashboard route used to store the organisation submitted on the create route above.
Route::post('/home/organisation/store', 'HomeController@storeOrganisation')->name('home.organisation.store');

