<div class='list-organisations'>

    <table width='100%' class='table table-hover'>
        <thead class="thead-dark">
            <tr>
                <th scope="col">Organisation</th>
                <th scope="col">Owner</th>
                <th scope="col">Subscribed</th>
                <th scope="col">Expires</th>
            </tr>
        </thead>
        <tbody>
            @foreach($organisations as $organisation)
            <tr>
                <td>{{ $organisation['name'] }}</td>
                <td>{{ $organisation['owner'] }}</td>
                <td>{{ $organisation['subscribed'] }}</td>
                <td>{{ $organisation['trial_end'] }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>

</div>
