<div class='create-organisation'>
    <form action='{{ route("home.organisation.store") }}' method='post'>
        
        @csrf
        
        <div class="form-group">
            <label for="organisation_name">Organisation Name *</label>
            <input type="text" name='organisation_name' id="organisation_name" class="form-control" required>
        </div>

        <button class="btn btn-primary" type="submit">Create Organisation</button>

    </form>                        
</div>
