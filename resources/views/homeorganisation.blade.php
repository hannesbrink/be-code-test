@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">

                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <h2>Organisations</h2>

                    <ul class='dashboard-menu-items'>
                        <li class='dashboard-menu-item'>
                            <a href='{{ route("home") }}' class="btn btn-primary" type="button">
                                Show All
                            </a>                                              
                        </li>
                        <li class='dashboard-menu-item'>
                            <a href='{{ route("home.organisation.create") }}' class="btn btn-primary active" type="button">
                                Create
                            </a>                      
                        </li>
                    </ul>

                    @include('organisations.create')

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
